*** Settings ***
Suite Setup       REPORT SET
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ========================================================================================================================================
Resource          TEST_KEYWORD.txt

*** Test Cases ***
TC001_AdvantageDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC002_ExexDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC003_XcartDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC004_ExamAbantecartDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC005_ExamIIShoppingDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC006_API
    RUN_EXECUTE    ${TEST_NAME}

TC00_GetJobJenkins
    RUN_EXECUTE    ${TEST_NAME}

TC001_DogRandomYingGet
    RUN_EXECUTE    ${TEST_NAME}

TC002_EmployeeYingPost
    RUN_EXECUTE    ${TEST_NAME}
