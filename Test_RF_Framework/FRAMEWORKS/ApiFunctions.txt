*** Settings ***
Resource          Library.txt

*** Keywords ***
[A] Get Request Data
    [Arguments]    ${URL}    ${AppName}    ${param}
    Set Library Search Order    RequestsLibrary
    Create Session    URL    ${URL}
    ${resp}    Get Request    URL    ${AppName}    params=${param}
    Log To Console    ${resp.status_code}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    resp-> ${resp}
    Log To Console    Michael-> ${resp.json()["ad"]["company"]}
    Should Be Equal As Strings    ${resp.json()["ad"]["company"]}    StatusCode Weekly

[A] Post Request Data
    [Arguments]    ${URL}    ${AppName}    ${Data}
    Set Library Search Order    RequestsLibrary
    &{headers}=    Create Dictionary    content-type=application/json    charset=utf-8    User-Agent=Mozilla/5.0
    Create Session    URL    ${URL}
    ${resp}    Post Request    URL    ${AppName}    data=${Data}    headers=${headers}
    Log To Console    ${resp}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    resp-->${resp}
    Log To Console    Status-->${resp.json()["status"]}
    Log To Console    name-->${resp.json()["data"]["name"]}
    Log To Console    Salary-->${resp.json()["data"]["salary"]}
    Log To Console    Age --> ${resp.json()["data"]["age"]}
    Should Be Equal As Strings    ${resp.json()["status"]}    success

[A] Get Request
    [Arguments]    ${URL}    ${AppName}    ${param}    ${Auth}
    Set Library Search Order    RequestsLibrary
    Create Session    URL    ${URL}    auth=${Auth}
    ${resp}    Get Request    URL    ${AppName}    params=${param}
    Log To Console    ${resp.status_code}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    resp-> ${resp}
    Log To Console    fullDisplayName-> ${resp.json()["fullDisplayName"]}
    Log To Console    url-> ${resp.json()["primaryView"]["url"]}
    Should Be Equal As Strings    ${resp.json()["fullDisplayName"]}    ORAWAN
    Should Be Equal As Strings    ${resp.json()["primaryView"]["url"]}    http://64.227.108.197:8080/job/ORAWAN/

[A] Get
    [Arguments]    ${URL}    ${AppName}
    Set Library Search Order    RequestsLibrary
    Create Session    URL    ${URL}
    ${resp}    Get Request    URL    ${AppName}
    Log To Console    ${resp.status_code}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    resp-> ${resp}
    Log To Console    Status--> ${resp.json()["status"]}
    Log To Console    Message--> ${resp.json()["message"]}
    Should Be Equal As Strings    ${resp.json()["status"]}    success
